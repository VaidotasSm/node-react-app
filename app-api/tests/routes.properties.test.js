'use strict';

const assert = require('chai').assert;
const app = require('../index');
const request = require('supertest');
const db = require('../common/db');
const { createProperty } = require('../common/dbDevTools');

describe('Route - api/properties', () => {

	const propertyFields1 = {
		owner: 'carlos',
		address: {
			line1: 'Flat 5',
			line4: '7 Westbourne Terrace',
			postCode: 'W2 3UL',
			city: 'London',
			country: 'U.K.',
		},
		airbnbId: 3512500,
		numberOfBedrooms: 1,
		numberOfBathrooms: 1,
		incomeGenerated: 2000.34,
	};

	const propertyFields2 = {
		owner: 'ankur',
		address: {
			line1: '4',
			line2: 'Tower Mansions',
			line3: 'Off Station road',
			line4: '86-87 Grange Road',
			postCode: 'SE1 3BW',
			city: 'London',
			country: 'U.K.',
		},
		airbnbId: 1334159,
		numberOfBedrooms: 3,
		numberOfBathrooms: 1,
		incomeGenerated: 10000,
	};

	const propertyFields3 = {
		owner: 'elaine',
		address: {
			line1: '4',
			line2: '332b',
			line4: 'Goswell Road',
			postCode: 'EC1V 7LQ',
			city: 'London',
			country: 'U.K.',
		},
		airbnbId: 12220057,
		numberOfBedrooms: 2,
		numberOfBathrooms: 2,
		incomeGenerated: 1200,
	};

	before(async () => {
		await db.syncModels();

		await createProperty(db.sequelize.models, propertyFields1, true);
		await createProperty(db.sequelize.models, propertyFields2, true);
		await createProperty(db.sequelize.models, propertyFields3, true);

	});

	describe('GET /', () => {

		it('should respond with correct body', async () => {
			const res = await request(app).get('/api/properties');
			assert.equal(res.status, 200);
			assert.equal(res.body.data.length, 3);
			assert.deepInclude(res.body._metadata.pagination, {
				page: 0,
				total_pages: 1,
			});
			assertPropertyResponse(res.body.data[0], propertyFields3);
			assertPropertyResponse(res.body.data[1], propertyFields2);
			assertPropertyResponse(res.body.data[2], propertyFields1);
		});

		it('should return correct pagination', async () => {
			const res = await request(app).get('/api/properties?page=0&pageSize=1');
			assert.equal(res.status, 200);
			assert.equal(res.body.data.length, 1);
			assert.deepEqual(res.body._metadata.pagination, {
				page: 0,
				size: 1,
				total: 3,
				total_pages: 3,
			});
		});

	});

	describe('GET /:propertyId', () => {

		it('should receive 404 if no property', async () => {
			const res = await request(app).get('/api/properties/9999');
			assert.equal(res.status, 404, JSON.stringify(res.body));
		});

		it('should GET one property', async () => {
			const propertyBody = {
				owner: 'Owner 1',
				address: {
					line1: 'Flat 5',
					line4: '7 Westbourne Terrace',
					postCode: 'W2 3UL',
					city: 'London',
					country: 'U.K.',
				},
				airbnbId: 3512500,
				numberOfBedrooms: 1,
				numberOfBathrooms: 1,
				incomeGenerated: 2000.34,
			};
			const property = await createProperty(db.sequelize.models, propertyBody);

			const res = await request(app).get(`/api/properties/${property.get('id')}`);
			assert.equal(res.status, 200, JSON.stringify(res.body));
			assert.equal(res.body.data.id, property.get('id'));
			assertPropertyResponse(res.body.data, propertyBody);
			assert.equal(res.body.data.history.length, 0);
		});

		it('should GET one property with history', async () => {
			const propertyBody = {
				owner: 'Owner 1',
				address: {
					line1: 'Flat 5',
					line4: '7 Westbourne Terrace',
					postCode: 'W2 3UL',
					city: 'London',
					country: 'U.K.',
				},
				airbnbId: 3512500,
				numberOfBedrooms: 1,
				numberOfBathrooms: 1,
				incomeGenerated: 2000.34,
			};
			const property = await createProperty(db.sequelize.models, propertyBody, true);

			const res = await request(app).get(`/api/properties/${property.get('id')}`);
			assert.equal(res.status, 200, JSON.stringify(res.body));
			assertPropertyResponse(res.body.data, propertyBody);
			assert.equal(res.body.data.history.length, 1);
		});

	});

	describe('POST /', () => {

		it('should persist property', async () => {
			const propertyBody = {
				owner: 'Owner 1',
				address: {
					line1: '44',
					line2: '333b',
					line3: 'L3',
					line4: 'Goswell Road',
					postCode: 'EC1V 7LQ',
					city: 'London',
					country: 'U.K.',
				},
				airbnbId: 12220057,
				numberOfBedrooms: 2,
				numberOfBathrooms: 2,
				incomeGenerated: 1200,
			};

			const res = await request(app).post('/api/properties')
				.send(propertyBody);
			assert.equal(res.status, 201, JSON.stringify(res.body));
			assertPropertyResponse(res.body.data, propertyBody);
			await assertPropertyInDb(res.body.data.id, propertyBody);
		});

		it('should return validation errors', async () => {
			const propertyBody = {
				owner: '    ',
				address: {
					line1: null,
					line2: '333b',
					line3: 'L3',
					line4: null,
					postCode: 'EC1V 7LQ',
					city: 'London',
					country: 'U.K.',
				},
				airbnbId: 12220057,
				numberOfBedrooms: 2,
				numberOfBathrooms: 0,
				incomeGenerated: 1200,
			};

			const res = await request(app).post('/api/properties')
				.send(propertyBody);
			assert.equal(res.status, 400, JSON.stringify(res.body));
			assert.deepEqual(res.body.message, 'You have some invalid fields');
			assert.deepEqual(res.body.fields, [
				{ field: 'owner', message: 'Cannot be empty' },
				{ field: 'address.line1', message: 'Cannot be empty' },
				{ field: 'address.line4', message: 'Cannot be empty' },
			]);
		});

	});

	describe('PUT /:propertyId', () => {

		it('should receive 404 if no property', async () => {
			const res = await request(app).put('/api/properties/9999').send({});
			assert.equal(res.status, 404, JSON.stringify(res.body));
		});

		it('should update property', async () => {
			const originalFields = {
				owner: 'Owner Old',
				address: {
					line1: 'L1',
					line2: 'L2',
					line3: 'L3',
					line4: 'L4',
					postCode: '1111',
					city: 'London',
					country: 'U.K.',
				},
				airbnbId: 1,
				numberOfBedrooms: 1,
				numberOfBathrooms: 1,
				incomeGenerated: 1000,
			};
			const updatedFields = {
				owner: 'Owner New',
				address: {
					line1: 'L1e',
					line2: 'L2e',
					line3: 'L3e',
					line4: 'L4e',
					postCode: '2222',
					city: 'Berlin',
					country: 'Ger',
				},
				airbnbId: 2,
				numberOfBedrooms: 2,
				numberOfBathrooms: 2,
				incomeGenerated: 2000,
			};
			const property = await createProperty(db.sequelize.models, originalFields);
			const res = await request(app).put(`/api/properties/${property.get('id')}`)
				.send(updatedFields);
			assert.equal(res.status, 200, JSON.stringify(res.body));
			await assertPropertyInDb(property.get('id'), updatedFields);
		});

		it('should return validation errors', async () => {
			const propertyBody = {
				owner: 'Owner 1',
				address: {
					line1: 'L1',
					line2: 'L2',
					line3: 'L3',
					line4: 'L4',
					postCode: 'EC1V 7LQ',
					city: 'London',
					country: 'U.K.',
				},
				airbnbId: 12220057,
				numberOfBedrooms: -1,
				numberOfBathrooms: 0,
				incomeGenerated: 0,
			};

			const res = await request(app).post('/api/properties')
				.send(propertyBody);
			assert.equal(res.status, 400, JSON.stringify(res.body));
			assert.deepEqual(res.body.message, 'You have some invalid fields');
			assert.deepEqual(res.body.fields, [
				{ field: 'numberOfBathrooms', message: 'Must be greater than zero' },
				{ field: 'incomeGenerated', message: 'Must be greater than zero' },
				{ field: 'numberOfBedrooms', message: 'Must be greater or equal to zero' },
			]);
		});

	});

	describe('DELETE /:propertyId', () => {

		it('should receive 404 if no property', async () => {
			const res = await request(app).delete('/api/properties/9999');
			assert.equal(res.status, 404, JSON.stringify(res.body));
		});

		it('should delete property', async () => {
			const property = await createProperty(db.sequelize.models, {
				owner: 'Owner 2',
				address: {
					line1: '45',
					line2: '333b',
					line4: 'Goswell Road',
					postCode: 'EC1V 7LQ',
					city: 'London',
					country: 'U.K.',
				},
				airbnbId: 12220057,
				numberOfBedrooms: 2,
				numberOfBathrooms: 2,
				incomeGenerated: 1200,
			});

			const res = await request(app).delete(`/api/properties/${property.get('id')}`);
			assert.equal(res.status, 204, JSON.stringify(res.body));

			const propertyDb = await db.sequelize.models.properties.findById(property.get('id'));
			assert.isNull(propertyDb, 'should delete from DB');
		});

	});

});

function assertPropertyResponse(propertyBody, expected) {
	const expectedProperty = { ...expected };
	const expectedAddress = expectedProperty.address;
	delete expectedProperty.address;

	assert.exists(propertyBody, 'body should contain property');
	assert.deepInclude(propertyBody, expectedProperty);
	assert.exists(propertyBody.address, 'body should contain property address');
	assert.deepInclude(propertyBody.address, expectedAddress);
}

async function assertPropertyInDb(propertyId, expected) {
	assert.exists(propertyId, 'propertyId should be provided');
	const snapshotsDb = await db.sequelize.models.propertySnapshots.findAll({
		where: {
			propertyId: {
				[db.Op.eq]: propertyId,
			},
			isActive: true,
		},
		include: [
			{ model: db.sequelize.models.addresses, required: true },
		],
	});

	assert.equal(snapshotsDb.length, 1, 'should be one version of property');
	const updatedDb = snapshotsDb[0].toJSON();
	delete updatedDb.address.id;
	delete updatedDb.address.createdAt;
	delete updatedDb.address.updatedAt;
	assert.deepInclude(updatedDb, expected);
}
