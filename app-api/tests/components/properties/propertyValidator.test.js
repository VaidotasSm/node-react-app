'use strict';

const assert = require('chai').assert;
const propertyValidator = require('../../../components/properties/propertyValidator');
const { shouldThrow } = require('../../common/commonAsserts');

describe('components/properties/propertyValidator', () => {
	const property = {
		owner: 'Owner1',
		address: {
			line1: 'L1',
			line2: 'L2',
			line3: 'L3',
			line4: 'L4',
			postCode: 'W2 3UL',
			city: 'London',
			country: 'U.K.',
		},
		airbnbId: 3512500,
		numberOfBedrooms: 1,
		numberOfBathrooms: 1,
		incomeGenerated: 2000.34,
	};

	it('should fail with empty property object', async () => {
		const err = shouldThrow(() => {
			propertyValidator.validateProperty(null);
		});
		assert.containsAllKeys(err, ['fields']);
		assert.deepEqual(err.fields.map(f => f.field), ['property']);
		err.fields.forEach(field => assert.equal(field.message, 'Cannot be empty'));
	});

	it('should fail with empty address object', async () => {
		const prop = { ...property, address: null };
		const err = shouldThrow(() => {
			propertyValidator.validateProperty(prop);
		});
		assert.containsAllKeys(err, ['fields']);
		assert.deepEqual(err.fields.map(f => f.field), ['address']);
		err.fields.forEach(field => assert.equal(field.message, 'Cannot be empty'));
	});

	it('should fail with mandatory fields missing', async () => {
		const prop = {
			owner: null,
			airbnbId: null,
			numberOfBedrooms: null,
			numberOfBathrooms: null,
			incomeGenerated: null,
			address: {
				line1: null,
				line2: null,
				line3: null,
				line4: null,
				postCode: null,
				city: null,
				country: null,
			},
		};

		const err = shouldThrow(() => {
			propertyValidator.validateProperty(prop);
		});
		assert.containsAllKeys(err, ['fields']);
		assert.deepEqual(err.fields.map(f => f.field), [
			'owner',
			'airbnbId',
			'numberOfBedrooms',
			'numberOfBathrooms',
			'incomeGenerated',
			'address.line1',
			'address.line4',
			'address.postCode',
			'address.city',
			'address.country',
		]);
		err.fields.forEach(field => assert.equal(field.message, 'Cannot be empty'));
	});

	it('should fail with empty string mandatory field', async () => {
		const prop = { ...property, owner: '    ' };
		const err = shouldThrow(() => {
			propertyValidator.validateProperty(prop);
		});
		assert.containsAllKeys(err, ['fields']);
		assert.deepEqual(err.fields.map(f => f.field), ['owner']);
		err.fields.forEach(field => assert.equal(field.message, 'Cannot be empty'));
	});

	it('should fail with negative values', async () => {
		const prop = {
			...property,
			numberOfBedrooms: -1,
			numberOfBathrooms: -10,
			incomeGenerated: -100,
		};
		const err = shouldThrow(() => {
			propertyValidator.validateProperty(prop);
		});
		assert.containsAllKeys(err, ['fields']);
		assert.deepEqual(err.fields.map(f => f.field), [
			'numberOfBathrooms', 'incomeGenerated', 'numberOfBedrooms',
		]);
		assert.deepEqual(err.fields.map(field => field.message), [
			'Must be greater than zero',
			'Must be greater than zero',
			'Must be greater or equal to zero',
		]);
	});

	it.only('should fail with non numeric values', async () => {
		const prop = {
			...property,
			numberOfBedrooms: 'aaa',
			numberOfBathrooms: 'djsfhdskjf',
			incomeGenerated: '99a',
		};
		const err = shouldThrow(() => {
			propertyValidator.validateProperty(prop);
		});
		assert.containsAllKeys(err, ['fields']);
		assert.deepEqual(err.fields.map(f => f.field), [
			'numberOfBathrooms', 'incomeGenerated', 'numberOfBedrooms',
		]);
	});

	it('should fail with zero values', async () => {
		const prop = {
			...property,
			numberOfBathrooms: 0,
			incomeGenerated: 0,
		};
		const err = shouldThrow(() => {
			propertyValidator.validateProperty(prop);
		});
		assert.containsAllKeys(err, ['fields']);
		assert.deepEqual(err.fields.map(f => f.field), [
			'numberOfBathrooms', 'incomeGenerated',
		]);
		err.fields.forEach(field => assert.equal(field.message, 'Must be greater than zero'));
	});

});
