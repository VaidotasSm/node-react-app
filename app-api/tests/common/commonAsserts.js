'use strict';

const assert = require('chai').assert;

exports.shouldThrowAsync = async function shouldThrowAsync(callback) {
	let success = false;
	try {
		await callback();
		success = true;
	} catch (err) {
		return err;
	}
	assert.isFalse(success, 'should have thrown');
};

exports.shouldThrow = function shouldThrow(callback) {
	let success = false;
	try {
		callback();
		success = true;
	} catch (err) {
		return err;
	}
	assert.isFalse(success, 'should have thrown');
};
