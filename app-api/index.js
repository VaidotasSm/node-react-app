const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const config = require('config');
const propertiesRouter = require('./routes/properties');
const db = require('./common/db');
const dbDevTools = require('./common/dbDevTools');
const { createLogger } = require('./common/logger');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/web', express.static(path.join(__dirname, '../app-frontend/dist')));

app.use('/api/properties', propertiesRouter);

const logger = createLogger();

app.use((req, res) => {
	res.status(404).json({ message: 'Not found' });
});
app.use((err, req, res, next) => {
	if (!err) {
		return next();
	}

	if (err.statusCode) {
		return res.status(err.statusCode).json({
			message: err.message || 'Something went wrong',
			fields: err.fields || [],
		});
	}

	logger.error(err, 'Unknown error occurred');
	res.status(500).json({ message: 'Something went wrong' });
});

if (config.db.syncModels) {
	(async () => {
		try {
			await db.syncModels();
			logger.info('DB synced...');

			await dbDevTools.seedRandomData(db.sequelize.models);
			logger.info('DB seeded...');
		} catch (err) {
			logger.error(err, 'DB error');
		}
	})();
}

app.listen(config.app.port, () => {
	logger.info('Application started...');
	logger.info({ NODE_ENV: process.env.NODE_ENV }, 'Application Settings');
});

module.exports = app;
