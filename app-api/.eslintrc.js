module.exports = {
	extends: 'airbnb',
	parserOptions: {
		'ecmaVersion': 2017
	},
	env: {
		es6: true,
		browser: true,
		node: true,
		mocha: true
	},
	rules: {
		'import/newline-after-import': 0,
		'max-len': [2, 120],
		'indent': [2, 'tab'],
		'no-tabs': 0,
		'no-unused-vars': 'warn',
		'no-console': 'error',
		'consistent-return': 0,
		'strict': 0,
		'padded-blocks': 0,
		'prefer-destructuring': 0,
		'no-use-before-define': 0,
		'no-underscore-dangle': 0,
	}
};
