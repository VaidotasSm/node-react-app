const express = require('express');
const router = express.Router();
const propertyService = require('../components/properties/propertyService');
const { tryRoute } = require('./utils/httpUtils');

router.get('/:propertyId', tryRoute(async (req, res) => {
	const property = await propertyService.getProperty(req.params.propertyId);
	if (!property) {
		return res.status(404).json({ message: 'Property not found' });
	}

	res.status(200).json({
		data: property,
	});
}));

router.get('/', tryRoute(async (req, res) => {
	const { properties, pagination } = await propertyService.getProperties({
		page: parseInt(req.query.page, 10) || 0,
		pageSize: parseInt(req.query.pageSize, 10) || 10,
	});

	res.status(200).json({
		data: properties,
		_metadata: {
			pagination: toPaginationResponse(pagination),
		},
	});
}));

router.post('/', tryRoute(async (req, res) => {
	const created = await propertyService.createProperty(req.body);
	res.status(201).json({ data: created });
}));

router.put('/:propertyId', tryRoute(async (req, res) => {
	const updated = await propertyService.updateProperty(req.params.propertyId, req.body);
	res.status(200).json({ data: updated });
}));

router.delete('/:propertyId', tryRoute(async (req, res) => {
	await propertyService.deleteProperty(req.params.propertyId);
	res.sendStatus(204);
}));


function toPaginationResponse({ page, pageSize, total }) {
	return {
		page,
		size: pageSize,
		total,
		total_pages: Math.ceil(total / pageSize),
	};
}

module.exports = router;
