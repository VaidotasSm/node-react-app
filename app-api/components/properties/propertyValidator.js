'use strict';

const errors = require('../../common/errors');
exports.validateProperty = function validateProperty(property) {
	if (!property) {
		throw toErr([InvalidField('property', 'Cannot be empty')]);
	}
	let emptyFields = checkEmptyFields(property, [
		'owner',
		'airbnbId',
		'numberOfBedrooms',
		'numberOfBathrooms',
		'incomeGenerated',
		'address',
	]);
	if (property.address) {
		const emptyAddressFields = checkEmptyFields(property.address, [
			'line1',
			'line4',
			'postCode',
			'city',
			'country',
		]).map(val => InvalidField(`address.${val.field}`, val.message));
		emptyFields = [
			...emptyFields,
			...emptyAddressFields,
		];
	}
	if (emptyFields.length > 0) {
		throw toErr(emptyFields);
	}

	const negativeFields = checkNegativeNumbers(property, ['numberOfBathrooms', 'incomeGenerated'], false);
	const negativeOrZeroFields = checkNegativeNumbers(property, ['numberOfBedrooms']);
	const errorFields = [...negativeFields, ...negativeOrZeroFields];
	if (errorFields.length > 0) {
		throw toErr(errorFields);
	}
};

function checkEmptyFields(object, fieldNames = []) {
	return fieldNames
		.filter(name => isEmpty(object[name]) || isEmptyString(object[name]))
		.map(name => InvalidField(name, 'Cannot be empty'));
}

function checkNegativeNumbers(object, fieldNames = [], allowZero = true) {
	return fieldNames
		.filter(name => {
			return !Number.isFinite(object[name]) ||
				(allowZero ? object[name] < 0 : object[name] <= 0);
		})
		.map(name => InvalidField(name, allowZero ? 'Must be greater or equal to zero' : 'Must be greater than zero'));
}

function isEmpty(value) {
	return !value && value !== 0;
}

function isEmptyString(value) {
	return typeof value === 'string' && !value.trim();
}

function InvalidField(field, message) {
	return { field, message };
}

function toErr(fields = []) {
	return errors.err(400, 'You have some invalid fields', fields);
}
