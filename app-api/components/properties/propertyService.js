'use strict';

const db = require('../../common/db');
const errors = require('../../common/errors');
const { validateProperty } = require('./propertyValidator');

exports.getProperties = async function getProperties(pagination = { page: 0, pageSize: 10 }) {
	const { rows, count } = await db.sequelize.models.propertySnapshots.findAndCountAll({
		where: {
			isActive: true,
		},
		include: [db.sequelize.models.addresses],
		order: [
			['createdAt', 'DESC'],
		],
		offset: pagination.page * pagination.pageSize,
		limit: pagination.pageSize,
	});
	return {
		properties: rows.map(snapshot => snapshotToPropertyDto(snapshot)),
		pagination: {
			page: pagination.page,
			pageSize: pagination.pageSize,
			total: count,
		},
	};
};

exports.getProperty = async function getProperty(propertyId) {
	const { current, history } = await getPropertySnapshots(propertyId);
	if (!current) {
		return null;
	}

	const dto = snapshotToPropertyDto(current);
	dto.history = history.map(h => snapshotToHistoryDto(h));
	return dto;
};

exports.createProperty = async function createProperty(property) {
	validateProperty(property);

	return db.sequelize.transaction(async (transaction) => {
		const created = await db.sequelize.models.properties.create({});
		const propertySnapshot = await created.createPropertySnapshot({ ...property, isActive: true }, {
			include: [db.sequelize.models.addresses],
			transaction,
		});
		return snapshotToPropertyDto(propertySnapshot);
	});
};

exports.updateProperty = async function updateProperty(propertyId, updatedProperty) {
	return db.sequelize.transaction(async (transaction) => {
		const property = await await db.sequelize.models.properties.findById(propertyId, { transaction });
		if (!property) {
			throw errors.err(404, 'Property not found');
		}
		validateProperty(updatedProperty);

		await db.sequelize.models.propertySnapshots.update({
			isActive: false,
		}, {
			where: {
				propertyId: {
					[db.Op.eq]: propertyId,
				},
			},
			transaction,
		});
		const propertySnapshot = await property.createPropertySnapshot({
			...updatedProperty,
			id: null,
			isActive: true,
			propertyId,
			createdAt: null,
			updatedAt: null,
			address: {
				...updatedProperty.address,
				id: null,
			},
		}, {
			include: [db.sequelize.models.addresses],
			transaction,
		});
		return snapshotToPropertyDto(propertySnapshot);
	});

};

exports.deleteProperty = async function deleteProperty(propertyId) {
	await db.sequelize.transaction(async (transaction) => {
		const property = await await db.sequelize.models.properties.findById(propertyId, { transaction });
		if (!property) {
			throw errors.err(404, 'Property not found');
		}

		await db.sequelize.models.properties.destroy({
			where: {
				id: {
					[db.Op.eq]: propertyId,
				},
			},
			transaction,
		});
	});
};

async function getPropertySnapshots(propertyId, transaction) {
	const propertySnapshots = await db.sequelize.models.propertySnapshots.findAll({
		where: {
			propertyId: {
				[db.Op.eq]: propertyId,
			},
		},
		order: [
			['createdAt', 'DESC'],
		],
		include: [db.sequelize.models.addresses],
		transaction,
	});
	if (propertySnapshots.length === 0) {
		return { current: null, history: [] };
	}

	const current = propertySnapshots.shift();
	return {
		current,
		history: propertySnapshots,
	};
}

function snapshotToPropertyDto(propertySnapshot) {
	const plain = propertySnapshot.toJSON();
	plain.id = plain.propertyId;
	delete plain.propertyId;
	return plain;
}

function snapshotToHistoryDto(propertySnapshot) {
	return propertySnapshot.toJSON();
}
