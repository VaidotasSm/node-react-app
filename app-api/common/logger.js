'use strict';

const bunyan = require('bunyan');
const config = require('config');

exports.createLogger = function createLogger() {
	let stream = config.logger.stream;
	if (config.logger.stream === 'process.stdout') {
		stream = process.stdout;
	}

	return bunyan.createLogger({
		name: config.logger.name,
		stream,
		level: config.logger.level,
	});
};
