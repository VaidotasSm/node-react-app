'use strict';

exports.createProperty = async function createProperty(models, fields, generatePrevSnapshot = false) {
	const property = await models.properties.create({});
	if (generatePrevSnapshot) {
		await property.createPropertySnapshot({ ...fields, owner: 'Prev Owner' }, {
			include: [models.addresses],
		});
		await property.createPropertySnapshot({ ...fields, isActive: true }, {
			include: [models.addresses],
		});
	} else {
		await property.createPropertySnapshot({ ...fields, isActive: true }, {
			include: [models.addresses],
		});
	}
	return property;
};

exports.seedRandomData = async function seedRandomData(models) {
	await exports.createProperty(models, {
		owner: 'carlos',
		address: {
			line1: 'Flat 5',
			line4: '7 Westbourne Terrace',
			postCode: 'W2 3UL',
			city: 'London',
			country: 'U.K.',
		},
		airbnbId: 3512500,
		numberOfBedrooms: 1,
		numberOfBathrooms: 1,
		incomeGenerated: 2000.34,
	});
	await exports.createProperty(models, {
		owner: 'ankur',
		address: {
			line1: '4',
			line2: 'Tower Mansions',
			line3: 'Off Station road',
			line4: '86-87 Grange Road',
			postCode: 'SE1 3BW',
			city: 'London',
			country: 'U.K.',
		},
		airbnbId: 1334159,
		numberOfBedrooms: 3,
		numberOfBathrooms: 1,
		incomeGenerated: 10000,
	}, true);
	await exports.createProperty(models, {
		owner: 'elaine',
		address: {
			line1: '4',
			line2: '332b',
			line4: 'Goswell Road',
			postCode: 'EC1V 7LQ',
			city: 'London',
			country: 'U.K.',
		},
		airbnbId: 12220057,
		numberOfBedrooms: 2,
		numberOfBathrooms: 2,
		incomeGenerated: 1200,
	}, true);
	await exports.createProperty(models, {
		owner: 'Owner 1',
		address: {
			line1: '66',
			line2: '333b',
			line4: 'Goswell Road',
			postCode: 'EC1V 7LQ',
			city: 'London',
			country: 'U.K.',
		},
		airbnbId: 12220057,
		numberOfBedrooms: 2,
		numberOfBathrooms: 2,
		incomeGenerated: 700,
	}, true);
	await exports.createProperty(models, {
		owner: 'Owner 2',
		address: {
			line1: '67',
			line2: '334b',
			line4: 'Goswell Road',
			postCode: 'EC1V 7LQ',
			city: 'London',
			country: 'U.K.',
		},
		airbnbId: 12220057,
		numberOfBedrooms: 1,
		numberOfBathrooms: 1,
		incomeGenerated: 300,
	}, true);
	await exports.createProperty(models, {
		owner: 'Owner 3',
		address: {
			line1: '68',
			line2: '334b',
			line4: 'Goswell Road',
			postCode: 'EC1V 7LQ',
			city: 'London',
			country: 'U.K.',
		},
		airbnbId: 12220057,
		numberOfBedrooms: 1,
		numberOfBathrooms: 2,
		incomeGenerated: 900,
	}, true);
};
