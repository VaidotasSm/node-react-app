'use strict';

const Sequelize = require('sequelize');
const config = require('config');

const sequelize = new Sequelize(config.db.name, config.db.username, config.db.password, config.db.options);

const Property = sequelize.define('properties', {
	id: {
		type: Sequelize.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true,
	},
}, {
	schema: 'rentals',
});

const PropertySnapshot = sequelize.define('propertySnapshots', {
	id: {
		type: Sequelize.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true,
	},
	propertyId: {
		type: Sequelize.INTEGER,
	},
	isActive: {
		type: Sequelize.BOOLEAN,
		allowNull: false,
		defaultValue: false,
	},
	addressId: {
		type: Sequelize.INTEGER,
	},
	owner: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	airbnbId: {
		type: Sequelize.BIGINT,
		allowNull: false,
	},
	numberOfBedrooms: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	numberOfBathrooms: {
		type: Sequelize.INTEGER,
		allowNull: false,
	},
	incomeGenerated: {
		type: Sequelize.DECIMAL(18, 2),
		allowNull: false,
	},
}, {
	schema: 'rentals',
});

const Address = sequelize.define('addresses', {
	id: {
		type: Sequelize.INTEGER,
		allowNull: false,
		primaryKey: true,
		autoIncrement: true,
	},
	line1: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	line2: {
		type: Sequelize.STRING,
	},
	line3: {
		type: Sequelize.STRING,
	},
	line4: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	postCode: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	city: {
		type: Sequelize.STRING,
		allowNull: false,
	},
	country: {
		type: Sequelize.STRING,
		allowNull: false,
	},
}, {
	schema: 'rentals',
});

Property.hasMany(PropertySnapshot, {
	foreignKey: 'propertyId',
	onDelete: 'cascade',
	onUpdate: 'cascade',
});
PropertySnapshot.belongsTo(Property, {
	foreignKey: 'propertyId',
	onDelete: 'cascade',
	onUpdate: 'cascade',
});

Address.hasMany(PropertySnapshot, {
	foreignKey: 'addressId',
	onDelete: 'cascade',
	onUpdate: 'cascade',
});
PropertySnapshot.belongsTo(Address, {
	foreignKey: 'addressId',
	onDelete: 'cascade',
	onUpdate: 'cascade',
});

module.exports = {
	sequelize,
	Sequelize,
	Op: Sequelize.Op,
	async syncModels() {
		await sequelize.sync({ force: true });
	},
};
