'use strict';

exports.err = function err(statusCode, message, fields = []) {
	return {
		statusCode,
		message,
		fields,
	};
};
