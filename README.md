# Node React App
Sample application for managing rental property.

## Technical

## Start App (non-dev mode)
Start application to check how it works.

* `cd app-api` - from root folder.

* `npm install`

* `npm run start`

* `cd app-frontend` - from root folder.

* `npm install`

* `npm run build`

* Go to  [http://localhost:8080/web/](http://localhost:8080/web/).

## Backend commands

* `cd app-api` - go to API directory.

* `npm install` - install dependencies.

Other commands

* `npm test` - run tests.

* `npm run start:dev-w` - start with `nodemon` watcher.

* `npx eslint .` - run ESLint.

## Frontend commands

* `cd app-frontend` - go to API directory.

* `npm run build` - build React app (production).

* `npm run start` - run in development mode, open [http://localhost:4200](http://localhost:4200).
