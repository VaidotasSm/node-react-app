'use strict';

import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Menu, Segment } from 'semantic-ui-react';
import 'react-notifications/lib/notifications.css';
import { NotificationContainer } from 'react-notifications';

class MainPageTemplate extends Component {

	redirect(route) {
		this.props.history.push(route);
	}

	isActive(path) {
		return this.props.location.pathname === path;
	}

	render() {
		return (
			<div>
				<Menu>
					<Menu.Item
						active={this.isActive('/')}
						onClick={() => this.redirect('/')}>
						Home
					</Menu.Item>
					<Menu.Item
						active={this.isActive('/properties')}
						onClick={() => this.redirect('/properties')}>
						Properties
					</Menu.Item>
				</Menu>
				<Segment basic>
					{this.props.children}
				</Segment>
				<NotificationContainer/>
			</div>
		);
	}
}

export default withRouter(MainPageTemplate);
