'use strict';

import React from 'react';
import { Segment } from 'semantic-ui-react';

const HomePage = (props) => {
	return (
		<div>
			<Segment basic>
				<h3>Manage Properties</h3>
				Application for managing imaginary Properties.
			</Segment>
		</div>
	);
};

export { HomePage };
