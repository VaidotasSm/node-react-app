'use strict';

import React, { Component } from 'react';
import { PropertyEdit } from '../../components/properties/PropertyEdit';
import { withRouter } from 'react-router-dom';
import { requestWithBody } from '../../common/request';
import { NotificationManager } from 'react-notifications';

class _PropertyCreatePage extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			property: {
				owner: '',
				airbnbId: '',
				numberOfBedrooms: 0,
				numberOfBathrooms: 0,
				incomeGenerated: 0,
				address: {
					line1: '',
					line2: '',
					line3: '',
					line4: '',
					postCode: '',
					city: '',
					country: '',
				}
			},
			errorFields: []
		};

		this.saveProperty = this.saveProperty.bind(this);
	}

	saveProperty(property) {
		requestWithBody('/properties', property, 'POST')
			.then((res) => {
				NotificationManager.success('', 'Successfully created Property');
				this.props.history.push('/properties');
			})
			.catch((err) => {
				const message = err.message || 'Something went wrong';
				let text = ``;
				if (err.fields && err.fields.length > 0) {
					text = `${err.fields[0].field}: ${err.fields[0].message}`;
					this.setState({ errorFields: err.fields });
				}
				NotificationManager.error(text, message);
			})
	}

	render() {
		return (
			<div>
				<h3>Create Property</h3>

				<PropertyEdit
					property={this.state.property}
					errorFields={this.state.errorFields}
					onSave={this.saveProperty}
				/>
			</div>
		);
	}
}

const PropertyCreatePage = withRouter(_PropertyCreatePage);
export { PropertyCreatePage };
