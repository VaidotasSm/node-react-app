'use strict';

import React, { Component } from 'react';
import { requestGet } from '../../common/request';
import { withRouter } from 'react-router-dom';
import { NotificationManager } from 'react-notifications';

import { PropertiesTable } from '../../components/properties/PropertiesTable';
import { Loading } from '../../components/ui/Loading';
import { Button, Segment } from 'semantic-ui-react';

const PAGE_SIZE = 5;

class _PropertiesPage extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {};

		this.selectProperty = this.selectProperty.bind(this);
		this.createProperty = this.createProperty.bind(this);
		this.loadPrevPage = this.loadPrevPage.bind(this);
		this.loadNextPage = this.loadNextPage.bind(this);
	}

	selectProperty(property) {
		this.props.history.push(`/properties/${property.id}`);
	}

	createProperty() {
		this.props.history.push('/properties/create');
	}

	componentDidMount() {
		this.setState({ isLoading: true });
		loadProperties(this);
	}

	loadPrevPage() {
		loadProperties(this, this.state.pagination.page + 1);
	}

	loadNextPage() {
		loadProperties(this, this.state.pagination.page - 1);
	}

	render() {
		if (this.state.isLoading || !this.state.properties) {
			return <Loading/>
		}

		const hasNext = this.state.pagination.total_pages > this.state.pagination.page + 1;
		const hasPrev = this.state.pagination.page > 0;
		const properties = <div>
			<PropertiesTable
				properties={this.state.properties}
				onSelect={this.selectProperty}
				onNext={hasNext ? this.loadPrevPage : null}
				onPrev={hasPrev ? this.loadNextPage : null}
			/>
		</div>;

		if (this.state.isLoading) {
			return <Loading/>
		}

		return (
			<div>
				<Segment basic textAlign='right'>
					<Button primary onClick={this.createProperty}>Add</Button>
				</Segment>
				{properties}
			</div>
		);
	}
}

function loadProperties(context, page = 0) {
	requestGet('/properties', { page: page, pageSize: PAGE_SIZE })
		.then((res) => {
			context.setState({
				properties: res.data,
				pagination: res._metadata.pagination,
				isLoading: false
			});
		})
		.catch((err) => {
			NotificationManager.error('Failed to load properties', 'Could not load data');
			context.setState({ isLoading: false });
		});
}

const PropertiesPage = withRouter(_PropertiesPage);
export {
	PropertiesPage
};
