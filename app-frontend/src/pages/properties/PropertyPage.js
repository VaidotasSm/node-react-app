'use strict';

import React, { Component } from 'react';
import { Button, Segment } from 'semantic-ui-react';
import { withRouter } from 'react-router';
import { requestGet, requestWithBody } from '../../common/request';
import { PropertyDisplay } from '../../components/properties/PropertyDisplay';
import { NotificationManager } from 'react-notifications';
import { PropertyHistoryDisplay } from '../../components/properties/PropertyHistoryDisplay';
import { Loading } from '../../components/ui/Loading';
import { PropertyEdit } from '../../components/properties/PropertyEdit';

const PageStatus = {
	PREVIEW: 'preview',
	HISTORY: 'history',
	EDIT: 'edit',
};

class _PropertyPage extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			pageStatus: PageStatus.PREVIEW,
		};

		this.edit = this.edit.bind(this);
		this.viewHistory = this.viewHistory.bind(this);
		this.viewInfo = this.viewInfo.bind(this);
		this.saveChanges = this.saveChanges.bind(this);
	}

	componentDidMount() {
		this.setState({ isLoading: true });
		requestGet(`/properties/${this.props.match.params.propertyId}`)
			.then((res) => {
				this.setState({ property: res.data, isLoading: false });
			})
			.catch((err) => {
				NotificationManager.error('Failed to load properties', 'Could not load data');
				this.setState({ isLoading: false });
			});
	}

	viewHistory() {
		this.setState({ pageStatus: PageStatus.HISTORY });
	}

	viewInfo() {
		this.setState({ pageStatus: PageStatus.PREVIEW });
	}

	edit() {
		this.setState({ pageStatus: PageStatus.EDIT });
	}

	saveChanges(property) {
		requestWithBody(`/properties/${this.props.match.params.propertyId}`, property, 'PUT')
			.then((res) => {
				NotificationManager.success(null, 'Successfully saved Property');
				this.props.history.push('/properties');
			})
			.catch((err) => {
				const message = err.message || 'Something went wrong';
				let text = ``;
				if (err.fields && err.fields.length > 0) {
					text = `${err.fields[0].field}: ${err.fields[0].message}`;
					this.setState({ errorFields: err.fields });
				}
				NotificationManager.error(text, message);
			})
	}

	render() {
		if (this.state.isLoading || !this.state.property) {
			return <Loading/>
		}

		let buttons;
		let content;
		if (this.state.pageStatus === PageStatus.EDIT) {
			const prop = {
				...this.state.property,
				address: { ...this.state.property.address }
			};
			content =
				<Segment basic>
					<PropertyEdit
						property={prop}
						errorFields={this.state.errorFields}
						onSave={this.saveChanges}
					/>
				</Segment>;
			buttons = <Segment basic textAlign='right'>
				<Button secondary onClick={this.viewInfo}>View Info</Button>
			</Segment>;
		} else if (this.state.pageStatus === PageStatus.HISTORY) {
			content = <div>
				<Segment basic textAlign='center'>
					<h3>History</h3>
				</Segment>
				<PropertyHistoryDisplay properties={this.state.property.history}/>
			</div>;
			buttons = <Segment basic textAlign='right'>
				<Button primary onClick={this.edit}>Edit</Button>
				<Button secondary onClick={this.viewInfo}>View Info</Button>
			</Segment>;
		} else {
			content = <PropertyDisplay property={this.state.property}/>;
			buttons = <Segment basic textAlign='right'>
				<Button primary onClick={this.edit}>Edit</Button>
				<Button secondary onClick={this.viewHistory}>View History</Button>
			</Segment>;
		}

		return (
			<Segment>
				{buttons}
				{content}
			</Segment>
		);
	}
}

const PropertyPage = withRouter(_PropertyPage);
export { PropertyPage };
