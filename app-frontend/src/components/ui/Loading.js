'use strict';

import React from 'react';
import { Loader, Segment } from 'semantic-ui-react';

const Loading = () => {
	return (
		<Segment basic textAlign='center'>
			<Loader active inline size='big'/>
		</Segment>
	);
};

export { Loading };
