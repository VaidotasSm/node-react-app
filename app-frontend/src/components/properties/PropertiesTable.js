'use strict';

import React from 'react';
import { Icon, Menu, Table } from 'semantic-ui-react';
import moment from 'moment/moment';

const PropertiesTable = ({ properties, onSelect, onNext, onPrev }) => {
	const rowStyle = onSelect ? { cursor: 'pointer' } : {};

	const body = properties ?
		properties.map((p) => (
			<Table.Row
				key={p.id}
				style={rowStyle}
				onClick={() => onSelect ? onSelect(p) : null}
			>
				<Table.Cell>
					{moment(p.createdAt).format('MMM Do YY HH:mm')}
				</Table.Cell>
				<Table.Cell>
					<span>
						{p.address ? `${p.address.line1} ${p.address.line2 || ''} ${p.address.line3 || ''} ${p.address.line4} ${p.address.city} ${p.address.country}` : 'N/A'}
					</span>
				</Table.Cell>
				<Table.Cell>
					{p.owner}
				</Table.Cell>
			</Table.Row>
		)) :
		<Table.Row>
			<Table.Cell colSpan='3' textAlign='center'>
				No records to display
			</Table.Cell>
		</Table.Row>;

	const footer =
		<Table.Footer>
			<Table.Row>
				<Table.HeaderCell colSpan='3'>
					<Menu floated='right' pagination>
						<Menu.Item as='a' icon disabled={!onPrev} onClick={onPrev}>
							<Icon name='chevron left'/>
						</Menu.Item>
						<Menu.Item as='a' icon disabled={!onNext} onClick={onNext}>
							<Icon name='chevron right'/>
						</Menu.Item>
					</Menu>
				</Table.HeaderCell>
			</Table.Row>
		</Table.Footer>;

	return (
		<Table selectable={!!onSelect}>
			<Table.Body>
				{body}
			</Table.Body>

			{footer}
		</Table>
	);
};

export { PropertiesTable };
