'use strict';

import React from 'react';
import { Divider, Grid } from 'semantic-ui-react';
import moment from 'moment';

const PropertyDisplay = ({ property }) => {
	if (!property) {
		return <div><span>Property not found</span></div>;
	}

	return (
		<div>
			<Grid>
				<Grid.Row>
					<Grid.Column textAlign='center' width={16}>
						<h3>Info</h3>
					</Grid.Column>
				</Grid.Row>
				{row('Created At', moment(property.createdAt).format('MMM Do YY HH:mm'))}
				{row('Owner', property.owner)}
				{row('Number of Bathrooms', property.numberOfBathrooms)}
				{row('Number Of Bedrooms', property.numberOfBedrooms)}
				{row('Income Generated', property.incomeGenerated)}
				{row('Airbnb', property.airbnbId ? <a href={`https://www.airbnb.com/rooms/${property.airbnbId}`}>link</a> : 'N/A' )}
				<Grid.Row>
					<Grid.Column textAlign='center' width={16}>
						<Divider/>
						<h3>Address</h3>
					</Grid.Column>
				</Grid.Row>
				{row('Line 1', property.address.line1)}
				{row('Line 2', property.address.line2)}
				{row('Line 3', property.address.line3)}
				{row('Line 4', property.address.line4)}
				{row('City', property.address.city)}
				{row('Country', property.address.country)}
			</Grid>
		</div>
	);
};

function row(name, renderObject) {
	return (
		<Grid.Row>
			<Grid.Column width={4}>
				<label>{name}</label>
			</Grid.Column>
			<Grid.Column width={12}>
				{renderObject}
			</Grid.Column>
		</Grid.Row>
	);
}

export { PropertyDisplay };
