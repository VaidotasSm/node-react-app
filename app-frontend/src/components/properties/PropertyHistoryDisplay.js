'use strict';

import React from 'react';
import { PropertiesTable } from './PropertiesTable';

const PropertyHistoryDisplay = ({ properties }) => {
	return (
		<div>
			<PropertiesTable properties={properties}/>
		</div>
	);
};

export { PropertyHistoryDisplay };
