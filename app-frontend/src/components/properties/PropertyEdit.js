'use strict';

import React, { Component } from 'react';
import { Button, Divider, Form, Message, Segment } from 'semantic-ui-react';

class PropertyEdit extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
			property: props.property
		};
		this.onPropChange = this.onPropChange.bind(this);
		this.onAddrChange = this.onAddrChange.bind(this);
		this.errorField = this.errorField.bind(this);
	}

	onPropChange(event) {
		const name = event.target.name;
		const convertToNumber = ['numberOfBedrooms', 'numberOfBathrooms', 'incomeGenerated']
			.includes(event.target.name);
		const value = convertToNumber ? (Number(event.target.value) || event.target.value) : event.target.value;
		this.setState((prevState) => {
			return {
				property: { ...prevState.property, [name]: value }
			}
		});
	}

	onAddrChange(event) {
		const name = event.target.name;
		const value = event.target.value;
		this.setState((prevState) => {
			return {
				property: {
				...prevState.property,
				address: {
					...prevState.property.address,
					[name]: value
				}
			}
			}
		});
	}

	errorField(fieldName) {
		if (!this.props.errorFields || this.props.errorFields.length === 0) {
			return false;
		}

		return this.props.errorFields.find((field) => field.field === fieldName);
	}

	render() {
		const property = this.state.property;
		const saveAction = this.props.onSave ? () => this.props.onSave(this.state.property) : () => undefined;
		return (
			<Segment>
				<Form>
					{Input(this, property.owner, 'owner', 'Owner', 'Owner name')}
					{Input(this, property.airbnbId, 'airbnbId', 'Airbnb ID')}

					<Form.Group widths='equal'>
						{Input(this, property.numberOfBathrooms, 'numberOfBathrooms', 'Number of Bathrooms')}
						{Input(this, property.numberOfBedrooms, 'numberOfBedrooms', 'Number Of Bedrooms')}
						{Input(this, property.incomeGenerated, 'incomeGenerated', 'Income Generated')}
					</Form.Group>

					<Divider/>
					<h3 align="center">Address</h3>

					{Input(this, property.address.line1, 'line1', 'Line 1', null, true)}
					{Input(this, property.address.line2, 'line2', 'Line 2', null, true)}
					{Input(this, property.address.line3, 'line3', 'Line 3', null, true)}
					{Input(this, property.address.line4, 'line4', 'Line 4', null, true)}

					<Form.Group widths='equal'>
						{Input(this, property.address.city, 'city', 'City', null, true)}
						{Input(this, property.address.country, 'country', 'Country', null, true)}
						{Input(this, property.address.postCode, 'postCode', 'Post Code', null, true)}
					</Form.Group>

					<Segment basic textAlign='right'>
						<Button primary onClick={saveAction}>Save</Button>
					</Segment>
				</Form>
			</Segment>
		);
	}
}

function Input(ctx, value, name, label, placeholder, isAddress = false) {
	const errorField = ctx.errorField(isAddress ? `address.${name}` : name);
	return <Form.Input
		error={!!errorField} fluid
		value={value} name={name} onChange={isAddress ? ctx.onAddrChange : ctx.onPropChange }
		label={label} placeholder={placeholder || label}
	/>;
}

export { PropertyEdit };
