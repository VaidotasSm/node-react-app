'use strict';

import 'whatwg-fetch';
import 'fetch-xhr';
import withQuery from 'with-query';

function requestGet(path, queryString = {}) {
	return fetch(withQuery(`http://localhost:8080/api${path}`, queryString), {
		headers: {
			'Content-Type': 'application/json'
		}
	})
		.then((res) => res.json().then((json) => ({ res, json })))
		.then(({res, json}) => {
			if (!res.ok) {
				throw json;
			}
			return json
		});
}

function requestWithBody(path, body, method = 'POST') {
	return fetch(`http://localhost:8080/api${path}`, {
		method: method,
		headers: {
			'Content-Type': 'application/json'
		},
		body: body ? JSON.stringify(body) : null
	})
		.then((res) => res.json().then((json) => ({ res, json })))
		.then(({res, json}) => {
			if (!res.ok) {
				throw json;
			}
			return json
		});
}

export {
	requestGet,
	requestWithBody
}
