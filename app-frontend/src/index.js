'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Switch } from 'react-router';
import { HashRouter } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import MainPageTemplate from './pages/MainPageTemplate';
import { HomePage } from './pages/HomePage';
import { PropertiesPage } from './pages/properties/PropertiesPage';
import { NotFoundPage } from './pages/NotFoundPage';
import { PropertyPage } from './pages/properties/PropertyPage';
import { PropertyCreatePage } from './pages/properties/PropertyCreatePage';

ReactDOM.render(
	<HashRouter>
		<MainPageTemplate>
			<Switch>
				<Route exact path='/' component={HomePage}/>
				<Route path='/properties/create' component={PropertyCreatePage}/>
				<Route path='/properties/:propertyId' component={PropertyPage}/>
				<Route path='/properties' component={PropertiesPage}/>
				<Route component={NotFoundPage}/>
			</Switch>
		</MainPageTemplate>
	</HashRouter>,
	document.getElementById('app')
);

// Hot Module Replacement
if (module.hot) {
	module.hot.accept();
}
